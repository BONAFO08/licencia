


const licenciaExistente = (verificacion) => {
    if (verificacion == true) {
        //x.style.display es una forma de usar/llamar propiedades css en js
        document.querySelector("#licenciaConduc").style.display = "inline";
        document.querySelector("#licenciaExistente").style.display = "none";
    } else {
        alert("Vas a tener que sacar una primero.");
    }
}

const compararDatos = () => {

    let nombre = document.querySelector("#nombreIn").value;
    let apellido = document.querySelector("#apellidoIn").value;
    let edad = parseInt(document.querySelector("#edadIn").value);
    let edadMayoria = 18;
    //Fecha de expiración / y transformarla en ms.
    let fechaIn = document.querySelector("#fechaIn").value;
    let fechaExp = new Date(fechaIn);
    let convertirFechaExp = fechaExp.getTime();
    //Obtener la fecha actual (en ms).
    let fechaActual = Date.now();
    //Auxiliares para el True/False.
    let auxEdad;
    let auxCarnet;

    // 1 == TRUE
    // 0 == FALSE

    if (edad >= edadMayoria) {
        auxEdad = "1";
    } else if (edad < edadMayoria) {
        auxEdad = "0";
    }

    if (convertirFechaExp <= fechaActual) {
        auxCarnet = "0";
    } else if (convertirFechaExp > fechaActual) {
        auxCarnet = "1";
    }

    //Concateno para dar mensajes personalizados con switch/case.
    let mostrarEnPantalla = auxEdad.concat(auxCarnet);
    let pantallaso = document.querySelector("#pantallaso");

    switch (mostrarEnPantalla) {
        //False-False.
        case "00":
            document.querySelector("#licenciaConduc").style.display = "none";
            document.querySelector("#licenciaExistente").style.display = "none";
            /*<font color = "">Para cambiar el color de un fragmento de txt
            no funciona si en css se coloca la propiedad "color"<font>
            */
            pantallaso.innerHTML = `<font color = "F17100">Señor/a </font><font color = "19BDFF">${nombre} ${apellido}</font><font color = "F17100">, para solicitar un nuevo carnet necesita la autorización de su/s tutor/es .</font>`;
            break;
        //True-False.
        case "10":
            document.querySelector("#licenciaConduc").style.display = "none";
            document.querySelector("#licenciaExistente").style.display = "none";
            pantallaso.innerHTML = `<font color = "F17100">Señor/a </font><font color = "19BDFF">${nombre} ${apellido}</font><font color = "F17100">,su carnet esta vencido, necesita solicitar uno nuevo.</font>`;
            break;
        //False-True.
        case "01":
            document.querySelector("#licenciaConduc").style.display = "none";
            document.querySelector("#licenciaExistente").style.display = "none";
            pantallaso.innerHTML = `<font color = "F17100">Señor/a </font><font color = "19BDFF">${nombre} ${apellido}</font><font color = "F17100">,su carnet esta en regla, pero es requerida la verificacion de su/s tutor/es.</font>`;
            break;
        //True-True.
        case "11":
            document.querySelector("#licenciaConduc").style.display = "none";
            document.querySelector("#licenciaExistente").style.display = "none";
            pantallaso.innerHTML = `<font color = "F17100">Señor/a </font><font color = "19BDFF">${nombre} ${apellido}</font><font color = "F17100">,sus credenciales están al día.</font>`;
            break;
        //En caso de error.
        default:
            document.querySelector("#licenciaConduc").style.display = "none";
            document.querySelector("#licenciaExistente").style.display = "none";
            pantallaso.innerHTML = `<font color = "#FF0000">Error...Contactando con el administrador...</font>`;
            break;
    }

}

//Botones
document.querySelector("#verificacionSI").addEventListener("click", () => {
    licenciaExistente(true);
});
document.querySelector("#verificacionNO").addEventListener("click", () => {
    licenciaExistente(false);
});

document.querySelector("#enviar").addEventListener("click", () => {
    compararDatos();
});